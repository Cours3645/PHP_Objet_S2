<!DOCTYPE HTML>
<html lang="en" ng-app='App'>
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=egde"/>
        <title>AngularJS App</title>

        <!-- LIBRARIES -->
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js'></script>
        <script src="https://code.angularjs.org/1.4.14/angular.min.js"></script>
        <script src="https://code.angularjs.org/1.4.14/angular-route.min.js"></script>
        <script src="https://code.angularjs.org/1.4.14/angular-resource.min.js"></script>
        <script src="https://code.angularjs.org/1.4.14/angular-sanitize.min.js"></script>
        <script src="http://nicelymusic.fr/vendor/angular-toastr/dist/angular-toastr.tpls.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

        <!-- STYLE / THEME -->
       
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href=" http://nicelymusic.fr/vendor/angular-toastr/dist/angular-toastr.css" rel="stylesheet">
        <link href="//cdncatalog.com/assets/flat-ui/css/flat-ui.css" rel="stylesheet">
        <link href="view/css/style.css" rel="stylesheet">

        <!-- THIS APP. -->
        <script src="controller/controllers.js"></script>
        <script src="model/Clients.js"></script>
        <script src="model/ClientAssurances.js"></script>
        <script src="model/Account.js"></script>
        <script src="App.js"></script>
    </head>

    <body class="container" ng-controller="mainCtrl">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#/Connexion" ng-if="!$root.authenticated" >Connexion</a></li>
                        <li><a ng-click="logout()" ng-if="$root.authenticated" >Deconnexion</a></li>
                        <li><a href="#/Clients" ng-if="$root.authenticated"  >Clients</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-sm-2">

                </div><!--/span-->
                <div class="col-sm-12">
                    <div class="jumbotron text-center">
                        <h2>Bienvenue sur le site de NBP TarifBas</h2>
                    </div>


                    <div class="row-fluid" ng-view><!-- VIEWS --></div>

                </div><!--/span-->
            </div><!--/row-->

        </div><!--/.fluid-container-->

    </body>
</html>
