<?php

require_once("Rest.inc.php");

error_reporting(1);

class API extends REST {

    public $data = "";
    private $DB_SERVER = "db619849416.db.1and1.com";
    private $DB_USER = "dbo619849416";
    private $DB_PASSWORD = "apqmwn134679";
    private $DB_NAME = "db619849416";
    //private $DB_NAME = "bdd_lazzer";
    private $objPDO = NULL;
    private $db = NULL;
    private $admin = false;

    public function __construct() {
        parent::__construct();                // Init parent contructor
        $this->dbConnect();                    // Initiate Database connection
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        try {
            $this->objPDO = new PDO('mysql:host=' . $this->DB_SERVER . ';dbname=' . $this->DB_NAME, $this->DB_USER, $this->DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

            $stmt = $this->objPDO->prepare("SET NAMES 'UTF8';");
            $stmt->execute();
            $stmt = $this->objPDO->prepare("SET CHARACTER SET utf8");
            $stmt->execute();
            $this->objPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        } catch (Exception $e) {
            echo 'Echec de la connexion à la base de données' . $e;
            exit();
        }
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     *
     */

    public function processApi() {
        $this->updateAuto();
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404); // If the method not exist with in this class, response would be "Page not found".
    }

    private function getAuthent() {

        list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['REDIRECT_HTTP_AUTHORIZATION'], 6)));

        if (($this->get_request_method() == "PUT") || ($this->get_request_method() == "POST") || ($this->get_request_method() == "DELETE")) {
            if (($_SERVER["PHP_AUTH_USER"] == "test") && ($_SERVER["PHP_AUTH_PW"] == "test")) {
                return true;
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
                $this->response($this->json($error), 401);
            }
        }
    }

    /*
     *    Simple login API
     *  Login must be POST method
     *  email : <USER EMAIL>
     *  pwd : <USER PASSWORD>
     */

    private function deleteUser() {
        // Cross validation if the request method is DELETE else it will return "Not Acceptable" status
        if ($this->get_request_method() != "DELETE") {
            $this->response('', 406);
        }
        $id = (int) $this->_request['id'];
        if ($id > 0) {
            mysql_query("DELETE FROM users WHERE user_id = $id");
            $success = array('status' => "Success", "msg" => "Successfully one record deleted.");
            $this->response($this->json($success), 200);
        } else
            $this->response('', 204);    // If no records "No Content" status
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                                            GET                                                                         */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function musique() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];

        if ($where != "") {
            $SQL = "SELECT *
      FROM musique
      WHERE " . $where;
        } else {
            $SQL = "SELECT *
      FROM musique";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function listeTop() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];

        if ($where != "") {
            $SQL = "SELECT *
      FROM top10Lazzer
      WHERE " . $where;
        } else {
            /* $SQL = "SELECT *
              FROM top10Lazzer, top10_musique, musique
              WHERE top10Lazzer.top10_id = top10_musique.top10_id
              AND musique.idMusique = top10_musique.idMusique
              ORDER BY top10_musique.top10_place"; */
            $SQL = "SELECT *
      FROM top10Lazzer ORDER BY top10_id DESC";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[$count] = $rlt;
                $result[$count]["listeMusique"] = $this->listeMusiqueTop($rlt["top10_id"]);
                $count ++;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function listeMusiqueTop($idTop) {

        $SQL = "SELECT *
    FROM top10_musique, musique
    WHERE musique.idMusique = top10_musique.idMusique
    AND top10_musique.top10_id = $idTop
    ORDER BY top10_musique.top10_place";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[$count] = $rlt;

                $result[$count]["artisteMusique"] = $this->nomArtisteMusique($rlt ["idMusique"], "auteur");
                if ($this->nomArtisteMusique($rlt ["idMusique"], "remix")) {
                    $result[$count]["artisteMusiqueRemix"] = $this->nomArtisteMusique($rlt ["idMusique"], "remix");
                } else {
                    $result[$count]["artisteMusiqueRemix"] = "";
                }
                if ($this->nomArtisteMusique($rlt ["idMusique"], "feat")) {
                    $result[$count]["artisteMusiqueFeat"] = $this->nomArtisteMusique($rlt ["idMusique"], "feat");
                } else {
                    $result[$count]["artisteMusiqueFeat"] = "";
                }

                $count++;
            }
            return $result;
        }
    }

    private function artiste() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $SQL = "";
        $where = htmlentities($_POST["where"], ENT_NOQUOTES);
        if ($where != "") {
            $SQL = "SELECT *
      FROM artiste
      WHERE " . $where . " ORDER BY artiste.nomArtiste";
        } else {
            $SQL = "SELECT *
      FROM artiste ORDER BY artiste.nomArtiste";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function post() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];
        if ($where != "") {
            $SQL = "SELECT *
      FROM post
      WHERE " . $where . " ORDER BY post.datePost";
        } else {
            $SQL = "SELECT *
      FROM post ORDER BY post.datePost";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function postActualite() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];

        if ($where != "") {
            $SQL = "SELECT *
      FROM postActualité, membregroupe
      WHERE " . $where . " AND postActualité.idMbGroupe = membregroupe.idMbGroupe";
        } else {
            $SQL = "SELECT *
      FROM postActualité ,membregroupe where postActualité.idMbGroupe = membregroupe.idMbGroupe";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function mbGroupe() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];
        if ($where != "") {
            $SQL = "SELECT *
      FROM membregroupe
      WHERE " . $where;
        } else {
            $SQL = "SELECT *
      FROM membregroupe";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function style() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];
        if ($where != "") {
            $SQL = "SELECT *
      FROM stylemusique
      WHERE " . $where . " ORDER BY nomStyle ASC ";
        } else {
            $SQL = "SELECT *
      FROM stylemusique ORDER BY nomStyle ASC";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function label() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];
        if ($where != "") {
            $SQL = "SELECT *
      FROM label
      WHERE " . $where . " ORDER BY nomLabel ASC ";
        } else {
            $SQL = "SELECT *
      FROM label ORDER BY nomLabel ASC";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function musiqueWithoutPost() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";

        $SQL = "SELECT distinct *
    FROM musique
    LEFT OUTER JOIN post ON musique.idMusique = post.idMusique
    where post.idMusique is null ";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function recentPost() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }
        $limit = $_GET["limit"];

        $query = "  SELECT DISTINCT *
    FROM post, musique, artiste, artiste_musique, membregroupe, stylemusique
    WHERE artiste_musique.idMusique = musique.idMusique
    AND artiste_musique.idArtiste = artiste.idArtiste
    AND post.idMusique = musique.idMusique
    AND stylemusique.idStyle = musique.idStyle
    GROUP BY musique.idMusique
    ORDER BY post.datePost
    LIMIT  " . $limit;

        $stmt = $this->objPDO->prepare($query);

        $stmt->execute();



        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function musiqueArtistePost() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }

        $SQL = "";
        $where = $_GET["where"];
        $min = $_GET["min"];
        $max = $_GET["max"];

        if ($where != "") {

            $SQL = "SELECT DISTINCT *
      FROM musique,
      artiste,
      artiste_musique,
      stylemusique
      WHERE artiste_musique.idMusique = musique.idMusique
      AND artiste_musique.idArtiste = artiste.idArtiste
      AND musique.idStyle = stylemusique.idStyle
      AND " . $where .
                    " GROUP BY artiste_musique.idMusique ORDER BY  musique.datePostMusique DESC, musique.datePreview  DESC";
        } else {

            $SQL = "SELECT DISTINCT(`idMusique`),`datePreview`,`dateReleases`,`datePostMusique`,`titreMusique`,`etat`,`lienWidget`,`lienSC`,`imgMusique`,`nomStyle`, musique.`idStyle`

    FROM musique LEFT JOIN stylemusique ON musique.idStyle = stylemusique.idStyle
    ORDER BY musique.datePostMusique DESC, musique.datePreview  DESC LIMIT $min , $max";
        }


        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        $sqlcount = "SELECT count('*') as nbMusique FROM musique";
        $stmt1 = $this->objPDO->prepare($sqlcount);
        $stmt1->execute();

        $nbMusique = $stmt1->fetch();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            $ipClient = $_SERVER["HTTP_USER_AGENT"] . " " . $_SERVER["REMOTE_ADDR"];

            while ($rlt = $stmt->fetch()) {
                $result[$count]['nbMusic'] = $nbMusique['nbMusique'];
                $result[$count]['idMusique'] = $rlt['idMusique'];
                $result[$count]['datePreview'] = $rlt['datePreview'];
                $result[$count]['dateReleases'] = $rlt['dateReleases'];
                $result[$count]['datePostMusique'] = $rlt['datePostMusique'];
                $result[$count]['titreMusique'] = $rlt['titreMusique'];
                $result[$count]['etat'] = $rlt['etat'];
                $result[$count]['idStyle'] = $rlt['idStyle'];
                $result[$count]['lienWidget'] = $rlt['lienWidget'];
                $result[$count]['lienSC'] = $rlt['lienSC'];
                $result[$count]['imgMusique'] = $rlt['imgMusique'];
                $result[$count]['nomStyle'] = $rlt['nomStyle'];
                $result[$count]['idStyle'] = $rlt['idStyle'];

                $result[$count]["artisteMusique"] = $this->nomArtisteMusique($rlt ["idMusique"], "auteur");
                if ($this->nomArtisteMusique($rlt ["idMusique"], "remix")) {
                    $result[$count]["artisteMusiqueRemix"] = $this->nomArtisteMusique($rlt ["idMusique"], "remix");
                } else {
                    $result[$count]["artisteMusiqueRemix"] = "";
                }
                if ($this->nomArtisteMusique($rlt ["idMusique"], "feat")) {
                    $result[$count]["artisteMusiqueFeat"] = $this->nomArtisteMusique($rlt ["idMusique"], "feat");
                } else {
                    $result[$count]["artisteMusiqueFeat"] = "";
                }


                $result[$count]["labelMusique"] = $this->labelMusique($rlt ["idMusique"]);

                $result[$count]["vote"] = $this->verifIpVote($rlt["idMusique"], $ipClient);
                $result[$count]["nbVote"] = $this->nbVote("musique.idMusique = " . $rlt["idMusique"]);
                $result[$count]["dataPost"] = $this->getPost("idMusique = " . $rlt["idMusique"]);

                $count++;
            }

            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    public function verifMusicArtistExist() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $musique = $_POST["musique"];
        $artiste = htmlentities($_POST["artiste"]);

        if ($musique != "" && $artiste != "") {
            $sql = 'SELECT * FROM `musique`,`artiste`,`artiste_musique`
      WHERE  `artiste`.idArtiste = `artiste_musique`.idArtiste
      AND `musique`.idMusique = `artiste_musique`.idMusique
      AND `musique`.titreMusique LIKE "%' . $musique . '%"
      AND `artiste`.nomArtiste = "' . $artiste . '"';
        }

        $stmt = $this->objPDO->prepare($sql);
        $stmt->execute();



        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            $this->response($this->json($result), 200);
        }
        $this->response('', 204);    // If no records "No Content" status
    }

    private function nbVote($where) {
        $SQL = "";

        if ($where != "") {
            $SQL = "SELECT count(*)
      FROM musique, vote
      WHERE musique.idMusique = vote.idMusique AND " . $where;
        } else {
            $SQL = "SELECT count(*)
      FROM musique,vote WHERE musique.idMusique = vote.idMusique";
        }


        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
        $rlt = $stmt->fetch();

        return $rlt[0];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                                            PUT                                          	                              */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function addMusique() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "PUT") {
            $this->response('', 406);
        }
        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }
        $dataMusique = $this->_request['dataMusique'];


        var_dump($dataMusique);
        if (!empty($dataMusique['titreMusique']) && !empty($dataMusique['datePreview']) && !empty($dataMusique['dateReleases']) && !empty($dataMusique['etat']) && !empty($dataMusique['idStyle']) && !empty($dataMusique['lienYT']) && !empty($dataMusique['lienSC']) && !empty($dataMusique['nomImage']) && !empty($dataMusique['idArtiste'])) {

            $datePreview = $dataMusique["datePreview"];
            $dateRealeases = $dataMusique["dateReleases"];
            $titreMusique = $dataMusique["titreMusique"];
            $etat = $dataMusique["etat"];
            $idStyle = $dataMusique["idStyle"];
            $lienWidget = $dataMusique["lienYT"];
            $lienSC = $dataMusique["lienSC"];
            $nomImage = $dataMusique["nomImage"];

            $idArtiste = $dataMusique["idArtiste"];
            $idArtisteRemix = $dataMusique["idArtisteRemix"];
            $idArtisteFeat = $dataMusique["idArtisteFeat"];
            $idLabel = $dataMusique["idLabel"];


            $requete = "INSERT INTO  `musique` " . "(`idMusique`, `datePreview`,`datePostMusique`, `dateReleases`, `titreMusique`, `etat`, `idStyle`, `lienWidget`, `lienSC`, `imgMusique`) VALUES
      ('NULL','" . $datePreview . "','" . $dateRealeases . "','" . $dateRealeases . "','" . addslashes($titreMusique) . "','" . $etat . "','" . $idStyle . "','" . $lienWidget . "','" . $lienSC . "','" . str_replace('\'', '', $nomImage) . "')";

            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();

            if ($stmt == true) {

                $lastId = $this->objPDO->lastInsertId();


                foreach ($idLabel as $ligne) {
                    $lien = "INSERT INTO`musique_label` (`idMusique`, `idLabel`) VALUES ('" . $lastId . "','" . $ligne . "')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtiste as $ligne) {
                    $lien = "INSERT INTO`artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $lastId . "','" . $ligne . "','auteur')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtisteRemix as $ligne) {
                    $lien = "INSERT INTO `artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $lastId . "','" . $ligne . "','remix')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtisteFeat as $ligne) {
                    $lien = "INSERT INTO `artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $lastId . "','" . $ligne . "','feat')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                $this->createThumbs("../../img/pochette/", "../../dev/img/pochette/thumbs200/", 200);
                $success = array('status' => "Success", "msg" => "Musique ajouter ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

    private function addPost() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "PUT") {
            $this->response('', 406);
        }
        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }

        $dataPost = $this->_request['dataPost'];
        if (!empty($dataPost['titrePost']) && !empty($dataPost['messagePost']) && !empty($dataPost['datePost']) && !empty($dataPost['idMusique']) && !empty($dataPost['idMbGroupe'])) {
            $titrePost = addslashes($dataPost['titrePost']);

            $messagePost = htmlentities($dataPost['messagePost']);
            $messagePost = addslashes(stripslashes(nl2br($messagePost)));
            $datePost = $dataPost["datePost"];
            $idMusique = $dataPost["idMusique"];
            $idMbGroupe = $dataPost["idMbGroupe"];


            $requete = "INSERT INTO `post` " . "(`idPost`, `titrePost`, `messagePost`, `datePost`, `idMusique`, `idMbGroupe`) VALUES
      ('NULL','" . $titrePost . "','" . $messagePost . "','" . $datePost . "','" . $idMusique . "','" . $idMbGroupe . "')";

            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();

            if ($stmt == true) {
                $success = array('status' => "Success", "msg" => "Post ajouter ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

    private function addArtiste() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "PUT") {
            $this->response('', 406);
        }

        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }

        $dataArtiste = $this->_request['dataArtiste'];
        if (!empty($dataArtiste['nomArtiste']) && !empty($dataArtiste['presentationArtiste']) && !empty($dataArtiste['nationaliterArtiste']) && !empty($dataArtiste['nomImage'])) {

            $nomArtiste = htmlentities($dataArtiste ["nomArtiste"]);
            $nomArtiste = addslashes(stripslashes(nl2br($nomArtiste)));



            $presentationArtiste = htmlentities($dataArtiste['presentationArtiste']);
            $presentationArtiste = addslashes(stripslashes(nl2br($presentationArtiste)));
            $nationaliter = $dataArtiste ["nationaliterArtiste"];
            $lienPhotoArtiste = $dataArtiste ["nomImage"];


            $requete = "INSERT INTO `artiste` " . "(`idArtiste`, `nomArtiste`, `presentation`, `nationaliter`, `lienPhotoArtiste`) VALUES
      ('NULL','" . $nomArtiste . "','" . $presentationArtiste . "','" . $nationaliter . "','" . $lienPhotoArtiste . "')";

            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();

            if ($stmt == true) {
                $success = array('status' => "Success", "msg" => "Artiste ajouter ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

    private function addPostActualite() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "PUT") {
            $this->response('', 406);
        }

        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }

        $dataPostActualite = $this->_request['dataPostActualite'];

        if (!empty($dataPostActualite['titrePost']) && !empty($dataPostActualite['messagePost']) && !empty($dataPostActualite['nomImage']) && !empty($dataPostActualite['idMbGroupe'])) {
            $titrePost = addslashes($dataPostActualite["titrePost"]);

            $descriptionPost = htmlentities($dataPostActualite['messagePost']);
            $descriptionPost = addslashes(stripslashes(nl2br($descriptionPost)));

            $imgPost = $dataPostActualite["nomImage"];
            $idMbGroupe = $dataPostActualite["idMbGroupe"];


            $requete = "INSERT INTO `postActualité` " . "(`idPost`, `titrePost`, `descriptionPost`, `datePost`, `imgPost`,`idMbGroupe`) VALUES
      ('NULL','" . $titrePost . "','" . $descriptionPost . "','" . date('ymd') . "','" . $imgPost . "','" . $idMbGroupe . "')";

            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();

            if ($stmt == true) {
                $success = array('status' => "Success", "msg" => "Post Actualite ajouter ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

    private function addVote() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "PUT") {
            $this->response('', 406);
        }

        if ($this->_request["idMusique"] != "") {
            $idMusique = $this->_request["idMusique"];
            $ipVote = $_SERVER["HTTP_USER_AGENT"] . " " . $_SERVER["REMOTE_ADDR"];

            if (!$this->verifIpVote($idMusique, $ipVote)) {

                // Ajout de l'article
                $query = "INSERT INTO `vote`(`idVote`, `idMusique`, `ipVote` ) VALUES ('','" . $idMusique . "','" . $ipVote . "');";
                $stmt = $this->objPDO->prepare($query);
                $stmt->execute();

                if ($stmt < 0) {
                    $success = array('status' => "Failed", "msg" => "Requette invalide");
                    $this->response($this->json($success), 406);
                } else {
                    $success = array('status' => "Success", "msg" => "Vote enregistré");
                    $this->response($this->json($success), 200);
                }
            } else {
                $error = array('status' => "Failed", "msg" => "Deja voter");
                $this->response($this->json($error), 406);
            }
            /* FIN IF */
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                                            POST                                                                        */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function updateAuto() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        $SQL = "UPDATE musique SET etat = 'releases' WHERE DATEDIFF(`dateReleases`,CURDATE()) <= 0 AND etat = 'preview'";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();


        $artiste = $this->getArtiste();
        foreach ($artiste as $value) {
            $SQL = "UPDATE artiste SET `nbMusiqueReleases` = (SELECT COUNT(*) FROM artiste_musique WHERE idArtiste = " . $value['idArtiste'] . " ) WHERE idArtiste = " . $value['idArtiste'];

            $stmt = $this->objPDO->prepare($SQL);
            $stmt->execute();
        }



        /* if($stmt->rowCount() > 0){
          $success = array('status' => "Success", "msg" => "Upadate ok");
          $this->response($this->json($success),200);
          }
          $this->response('',204);    // If no records "No Content" status */
    }

    private function updateMusique() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }
        $dataMusique = $this->_request['dataMusique'];

        //var_dump($dataMusique['titreMusique']);
        if (!empty($dataMusique['titreMusique']) && !empty($dataMusique['datePreview']) && !empty($dataMusique['dateReleases']) && !empty($dataMusique['etat']) && !empty($dataMusique['idStyle']) && !empty($dataMusique['lienWidget']) && !empty($dataMusique['lienSC']) && !empty($dataMusique['imgMusique']) && !empty($dataMusique['idArtiste'])) {
            $idMusique = $dataMusique["idMusique"];
            $datePreview = $dataMusique["datePreview"];
            $dateRealeases = $dataMusique["dateReleases"];
            $titreMusique = $dataMusique["titreMusique"];
            $etat = $dataMusique["etat"];
            $idStyle = $dataMusique["idStyle"];
            $lienWidget = $dataMusique["lienWidget"];
            $lienSC = $dataMusique["lienSC"];
            $nomImage = $dataMusique["imgMusique"];

            $idArtiste = $dataMusique["idArtiste"];
            $idArtisteRemix = $dataMusique["idArtisteRemix"];
            $idArtisteFeat = $dataMusique["idArtisteFeat"];
            $idLabel = $dataMusique["idLabel"];

            $requete = "UPDATE  `musique` SET `datePreview` = '$datePreview',
    `dateReleases` = '$dateRealeases',
    `titreMusique` = '$titreMusique',
    `etat` = '$etat',
    `idStyle` = $idStyle,
    `lienWidget` = '$lienWidget',
    `lienSC` = '$lienSC',
    `imgMusique` = '$nomImage'
    WHERE `idMusique` = $idMusique";


            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();


            if ($stmt == true) {
                $sql = "DELETE FROM `musique_label` WHERE `idMusique` = $idMusique ";
                $stmt = $this->objPDO->prepare($sql);
                $stmt->execute();

                $sql = "DELETE FROM `artiste_musique` WHERE `idMusique` = $idMusique ";
                $stmt = $this->objPDO->prepare($sql);
                $stmt->execute();


                foreach ($idLabel as $ligne) {
                    $lien = "INSERT INTO`musique_label` (`idMusique`, `idLabel`) VALUES ('" . $idMusique . "','" . $ligne . "')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtiste as $ligne) {
                    $lien = "INSERT INTO`artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $idMusique . "','" . $ligne . "','auteur')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtisteRemix as $ligne) {
                    $lien = "INSERT INTO `artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $idMusique . "','" . $ligne . "','remix')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }

                foreach ($idArtisteFeat as $ligne) {
                    $lien = "INSERT INTO `artiste_musique` (`idMusique`, `idArtiste`,`auteurOUremix`) VALUES ('" . $idMusique . "','" . $ligne . "','feat')";

                    $stmt = $this->objPDO->prepare($lien);
                    $stmt->execute();
                }
                $this->createThumbs("../../img/pochette/", "../../dev/img/pochette/thumbs200/", 200);
                $success = array('status' => "Success", "msg" => "Musique Editer ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

    private function updateArtiste() {

        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }

        $dataArtiste = $this->_request['dataEditArtiste'];

        if (!empty($dataArtiste['nomArtiste']) && !empty($dataArtiste['presentation']) && !empty($dataArtiste['nationaliter']) && !empty($dataArtiste['lienPhotoArtiste'])) {
            $nomArtiste = htmlentities($dataArtiste ["nomArtiste"]);
            $nomArtiste = addslashes(stripslashes(nl2br($nomArtiste)));
            $idArtiste = $dataArtiste["idArtiste"];
            $presentationArtiste = htmlentities($dataArtiste['presentation']);
            $presentationArtiste = addslashes(stripslashes(nl2br($presentationArtiste)));
            $nationaliter = $dataArtiste ["nationaliter"];
            $lienPhotoArtiste = $dataArtiste ["lienPhotoArtiste"];

            $requete = "UPDATE  `artiste` SET `nomArtiste` = '$nomArtiste',
    `presentation` = '$presentationArtiste',
    `nationaliter` = '$nationaliter',
    `lienPhotoArtiste` = '$lienPhotoArtiste'
    WHERE `idArtiste` = $idArtiste";

            $stmt = $this->objPDO->prepare($requete);
            $stmt->execute();

            if ($stmt == true) {
                $success = array('status' => "Success", "msg" => "Artiste ajouter ");
                $this->response($this->json($success), 200);
            } else {
                $error = array('status' => "Failed", "msg" => "Erreur requete");
                $this->response($this->json($error), 406);
            }
        } else {
            $error = array('status' => "Failed", "msg" => "Champ manquant");
            $this->response($this->json($error), 406);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                                            DELETE                                                                      */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /* DELETE artiste_musique.* FROM artiste_musique LEFT JOIN musique USING(idMusique) WHERE musique.idMusique IS NULL; */



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                                            INTERNE                                                                     */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function nomArtisteMusique($idMusique, $type) {
        $SQL = " SELECT artiste.idArtiste, artiste.nomArtiste
  FROM musique,artiste,artiste_musique
  WHERE artiste_musique.idMusique = musique.idMusique
  AND artiste_musique.idArtiste = artiste.idArtiste
  AND musique.`idMusique` =  " . $idMusique . "
  AND artiste_musique.auteurOUremix = '" . $type . "'";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[$count]['idArtiste'] = $rlt['idArtiste'];
                $result[$count]['nomArtiste'] = $rlt['nomArtiste'];
                $count++;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            return $result;
        }
    }

    private function labelMusique($idMusique) {
        $SQL = "  SELECT nomLabel, musique_label.idLabel
  FROM musique,label,musique_label
  WHERE musique_label.idMusique = musique.idMusique
  AND musique_label.idLabel = label.idLabel
  AND musique.`idMusique` =  " . $idMusique . "";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
        $count = 0;
        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[$count]["nomLabel"] = $rlt["nomLabel"];
                $result[$count]["idLabel"] = $rlt["idLabel"];
                $count++;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            return $result;
        }
    }

    private function nomArtisteMusiqueRemix($idMusique) {
        $SQL = "  SELECT *
  FROM musique,artiste,artiste_musique
  WHERE artiste_musique.idMusique = musique.idMusique
  AND artiste_musique.idArtiste = artiste.idArtiste
  AND musique.`idMusique` =  " . $idMusique . "
  AND artiste_musique.auteurOUremix = 'remix'";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            // If success everythig is good send header as "OK" and return list of users in JSON format
            return $result;
        }
    }

    private function getPost($where) {
        $SQL = "";

        if ($where != "") {
            $SQL = "SELECT *
    FROM post
    WHERE " . $where . " ORDER BY post.datePost";
        } else {
            $SQL = "SELECT *
    FROM post ORDER BY post.datePost";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            return $result;
        }
    }

    private function getArtiste($where) {
        $SQL = "";

        if ($where != "") {
            $SQL = "SELECT idArtiste
    FROM artiste
    WHERE " . $where;
        } else {
            $SQL = "SELECT idArtiste
    FROM artiste ";
        }

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            return $result;
        }
    }

    private function uploadImg() {
        if ($this->getAuthent() != true) {
            $this->response('', 401);
        }


        if (isset($_GET["typeImg"]) && isset($_FILES["file"])) {

            if ($_GET["typeImg"] == "pochette") {
                $tempDir = "../../img/pochette/";
            }
            if ($_GET["typeImg"] == "photoPostActualite") {
                $tempDir = "../../img/post/";
            }
            if ($_GET["typeImg"] == "photoArtiste") {
                $tempDir = "../../img/Artiste/";
            }

            $tmp = $_FILES["file"]["tmp_name"];

            move_uploaded_file($tmp, $tempDir . $_POST["nomImage"]);
        }

        if (isset($_POST["url"])) {
            if ($_GET["typeImg"] == "pochette") {
                $tempDir = "../../img/pochette/";
            }
            if ($_GET["typeImg"] == "photoPostActualite") {
                $tempDir = "../../img/post/";
            }
            if ($_GET["typeImg"] == "photoArtiste") {
                $tempDir = "../img/Artiste/";
            }

            $file = file_get_contents($_POST["url"]);

            file_put_contents($tempDir . $_POST["nomImage"], $file);
        }
    }

    private function verifIpVote($idMusique, $ipVote) {
        $query = "SELECT * FROM vote WHERE idMusique = $idMusique AND ipVote = '$ipVote'";
        $stmt = $this->objPDO->prepare($query);
        $stmt->execute();


        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function createThumbs($pathToImages, $pathToThumbs, $thumbWidth) {
        // open the directory
        // loop through it, looking for any/all JPG files:

        if (is_dir($pathToImages)) {

            if ($dh = opendir($pathToImages)) {

                while (($fname = readdir($dh)) !== FALSE) {




                    if ($fname != '.' && $fname != "..") {

                        $info = getimagesize($pathToImages . $fname);

                        if ($info !== false) {

                            $type = $info[2];
                            $width = $info[0]; // you don't need to use the imagesx and imagesy functions
                            $height = $info[1];

                            switch ($type) {
                                case IMAGETYPE_JPEG:
                                    $source_image = imagecreatefromjpeg($pathToImages . $fname);
                                    break;
                                case IMAGETYPE_GIF:
                                    $source_image = imagecreatefromgif($pathToImages . $fname);
                                    break;
                                case IMAGETYPE_PNG:
                                    $source_image = imagecreatefrompng($pathToImages . $fname);
                                    break;
                                default:
                                    throw new Exception('This file is not in JPG, GIF, or PNG format!');
                            }

                            $newWidth = $thumbWidth;


                            /* find the "desired height" of this thumbnail, relative to the desired width  */
                            $newHeight = floor($height * ($newWidth / $width));

                            $new = imagecreatetruecolor($newWidth, $newHeight);

                            if ($type == 1 or $type == 3) {
                                imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
                                imagealphablending($new, false);
                                imagesavealpha($new, true);
                            }


                            imagecopyresampled($new, $source_image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

                            switch ($type) {
                                case '6': imagewbmp($new, "{$pathToThumbs}{$fname}");
                                    break;
                                case '1': imagegif($new, "{$pathToThumbs}{$fname}");
                                    break;
                                case '2': imagejpeg($new, "{$pathToThumbs}{$fname}");
                                    break;
                                case '3': imagepng($new, "{$pathToThumbs}{$fname}");
                                    break;
                            }
                        }
                    }
                }
                // close the directory
                closedir($dir);
            }
        }
    }

    /*
     *    Encode array into JSON
     */

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

}

// Initiiate Library

$api = new API;
$api->processApi();
?>
