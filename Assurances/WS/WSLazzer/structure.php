<?php

class Musique {

    public $artisteMusique;
    public $dataPost;
    public $datePreview;
    public $dateReleases;
    public $etat;
    public $idArtiste;
    public $idMusique;
    public $idStyle;
    public $lienPhotoArtiste;
    public $lienSC;
    public $lienWidget;
    public $nationaliter;
    public $nbMusiqueReleases;
    public $nomArtiste;
    public $nomStyle;
    public $presentationtur;
    public $titreMusique;

    public function __construct() {
        
    }

}

class Artiste {

    public $idArtiste;
    public $nomArtiste;
    public $dataMusique;
    public $presentation;
    public $nbMusiqueReleases;
    public $nationaliter;
    public $lienPhotoArtiste;

    public function __construct() {
        
    }

}

class ArtisteMusique {

    public $idArtiste;
    public $idMusique;

    public function __construct() {
        
    }

}

class Post {

    public $idPost;
    public $titrePost;
    public $messagePost;
    public $datePost;
    public $idMusique;
    public $idMbGroupe;

    public function __construct() {
        
    }

}

class SetFavorie {

    public $idSet;
    public $dateSet;
    public $lieuSet;
    public $idArtiste;

    public function __construct() {
        
    }

}

class MbGroupe {

    public $idMbGroupe;
    public $nom;
    public $prenom;
    public $nbPost;

    public function __construct() {
        
    }

}

class SetFavArtiste {

    public $idArtiste;
    public $idsetFavori;

    public function __construct() {
        
    }

}

class StyleMusique {

    public $idStyle;
    public $nomStyle;

    public function __construct() {
        
    }

}
