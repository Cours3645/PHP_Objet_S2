<?php

require_once("Rest.inc.php");

error_reporting(0);

class API extends REST {

    public $data = "";
    private $DB_SERVER = "localhost";
    private $DB_USER = "user_assurance";
    private $DB_PASSWORD = "1234";
    private $DB_NAME = "bdd_nbp_tarifbas";
    //private $DB_NAME = "bdd_lazzer";
    private $objPDO = NULL;
    private $db = NULL;
    private $admin = false;

    public function __construct() {
        parent::__construct();                // Init parent contructor
        $this->dbConnect();                    // Initiate Database connection
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        try {
            $this->objPDO = new PDO('mysql:host=' . $this->DB_SERVER . ';dbname=' . $this->DB_NAME, $this->DB_USER, $this->DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

            $stmt = $this->objPDO->prepare("SET CHARACTER SET utf8");
            $stmt->execute();
        } catch (Exception $e) {
            echo 'Echec de la connexion à la base de données' . $e;
            exit();
        }
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     *
     */

    public function processApi() {
        //$this->updateAuto();
        $func = strtolower(trim(str_replace("/", "", $_GET['rquest'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404); // If the method not exist with in this class, response would be "Page not found".
    }

    /*
     *    Encode array into JSON
     */

    private function json($data) {
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    public function makeArray($SQL) {
        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            $this->json($result);
        }
    }

    public function getCustomerAssurances() {

        $idClient = $_GET['idClient'];

        $SQL = "SELECT * FROM assurances, type_assurances WHERE assurances.type_id = type_assurances.idTypeAssurance AND assurances.customer_id = " . $idClient;
        $this->makeArray($SQL);
    }

    public function getCustomerCars() {

        $idClient = $_GET['idClient'];

        $SQL = "SELECT * FROM car, customer WHERE car.customer_id = customer.customer_id AND customer.customer_id = " . $idClient;
        $this->makeArray($SQL);
    }

    public function getClients() {
        $SQL = "SELECT customer.customer_id, person.person_id, person.nom, person.prenom, person.email, person.telephone"
                . " FROM customer, person WHERE customer.person_id = person.person_id";
        $this->makeArray($SQL);
    }

    public function getClient() {

        $idClient = $_GET['idClient'];

        $SQL = "SELECT customer.customer_id, person.person_id, person.nom, person.prenom, person.email, person.telephone"
                . " FROM customer, person WHERE customer.person_id = person.person_id"
                . " AND customer.customer_id = " . $idClient;
        

        $this->makeArray($SQL);
    }

    public function updateCustomer() {

        $nom = $_POST["data"]["nom"];
        $prenom = $_POST["data"]["prenom"];
        $email = $_POST["data"]["email"];
        $telephone = $_POST["data"]["telephone"];
        $id = $_POST["data"]["person_id"];

        $SQL = "UPDATE  `person` SET `nom` = '$nom',
        `prenom` = '$prenom',
        `email` = '$email',
        `telephone` = $telephone
        WHERE `person_id` = $id";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt == true) {
            $success = array('status' => "Success", "msg" => "Clients Editer ");
            $this->json($success);
        }
    }

    public function addCustomer() {

        $nom = $_POST["data"]["nom"];
        $prenom = $_POST["data"]["prenom"];
        $email = $_POST["data"]["email"];
        $telephone = $_POST["data"]["telephone"];

        $SQL = "INSERT INTO `person` (`person_id`, `nom`, `prenom`, `email`, `telephone`) VALUES (NULL, '$nom', '$prenom', '$email', '$telephone')";


        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        $id = $this->objPDO->lastInsertId();

        $SQL = "INSERT INTO `customer` (`customer_id`, `person_id` ) VALUES (NULL, '$id')";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt == true) {

            $success = array('status' => "Success", "msg" => "Clients Editer ");
            $this->json($success);
        }
    }

    public function toggleAssu() {

        $idClient = $_GET['idClient'];
        $assuState = $_GET['assuState'];
        $typeAssu = $_GET['typeAssu'];
        
        $SQL = "SELECT idTypeAssurance FROM type_assurances WHERE Nom = '$typeAssu' ORDER BY idTypeAssurance DESC LIMIT 1";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
        }

        $idTypeAssurance = $result[0]["idTypeAssurance"];
        
        if($assuState == "true"){
            $SQL = "DELETE FROM assurances WHERE customer_id = $idClient AND type_id = $idTypeAssurance";
        }else{
            $SQL = "INSERT INTO assurances(assurance_id, customer_id, type_id) VALUES (NULL,$idClient,$idTypeAssurance)";
        }
        var_dump($SQL);
        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
    }
    
    public function deleteCar() {

        $idCar = $_GET['idCar'];
        
        $SQL = "SELECT assurance_id FROM car WHERE car_id = '$idCar' ORDER BY car_id DESC LIMIT 1";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
        }

        $idAssurance = $result[0]["assurance_id"];
        
        $SQL = "DELETE FROM car WHERE car_id = $idCar";
        
        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
        
        $SQL = "DELETE FROM assurances WHERE assurance_id = $idAssurance";
        
        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
    }
    
    public function addCar() {

        $nom = $_POST["data"]["addCarName"];
        $immat = $_POST["data"]["addCarImmat"];
        $customerId = $_POST["data"]["customer_id"];
        
        
        $SQL = "SELECT idTypeAssurance FROM type_assurances WHERE Nom = 'Voiture' ORDER BY idTypeAssurance DESC LIMIT 1";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $result = array();
            $count = 0;
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
        }

        $idTypeAssurance = $result[0]["idTypeAssurance"];
        
        $SQL = "INSERT INTO assurances(assurance_id, customer_id, type_id) VALUES (NULL,'$customerId','$idTypeAssurance')";
        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        $assuranceId = $this->objPDO->lastInsertId();
        
        var_dump($assuranceId);
        
        $SQL = "INSERT INTO `car` (`car_id`, `modele`, `immatriculation`, `assurance_id`, `customer_id`) VALUES (NULL, '$nom', '$immat', '$assuranceId', '$customerId');";


        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();

        if ($stmt == true) {

            $success = array('status' => "Success", "msg" => "Clients Editer ");
            $this->json($success);
        }
    }
    
    public function auth(){
        $usermame = $_POST["data"]["usename"];
        $password = $_POST["data"]["password"];
  
        
        $SQL = "SELECT username, user_id FROM user WHERE username = '$usermame' AND password = '$password'";

        $stmt = $this->objPDO->prepare($SQL);
        $stmt->execute();
        
 
        if ($stmt->rowCount() > 0) {
            while ($rlt = $stmt->fetch()) {
                $result[] = $rlt;
            }
            session_start();
            $_SESSION["SessionData"] = $result;
            
            $this->json($result);
            
            
            $success = array('status' => "Success", "msg" => "Connecté ");
            $this->json($success);
        } else {
            $error = array('status' => "Error", "msg" => "Mauvais mot de passe ou login ");
           $this->response($this->json($error), 401);
        }
    }
    
    public function getSession() {
        session_start();
        if(isset($_SESSION["SessionData"])){
            $this->json($_SESSION["SessionData"]);
        } else {
            $error = array('status' => "Error", "msg" => "Vous n'êtes pas conncté");
           $this->response($this->json($error), 401);
        }
        
    }
    
    public function logout() {
        session_start();
        session_destroy();
        session_abort();
        
    }
    
    
    

}

// Initiiate Library

$api = new API;
$api->processApi();
?>
