(function () {

    "use strict";

    var App = angular.module("App", [
        'toastr',
        "App.controllers",
        "App.ClientsService",
        "App.ClientAssurancesService",
        "App.AccountService",
        "ngRoute",
        "ngResource"
    ]);
    App.run(function ($rootScope, $location, Account, toastr) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $rootScope.authenticated = false;
            Account.getSession().then(function (results) {
                var test = $location.url()
                if (results[0].user_id || test  == '/Connexion')  {
                    $rootScope.authenticated = true;
                    $rootScope.user_id = results[0].user_id;
                    $rootScope.username = results[0].username;
                } else {
                    toastr.warning("Veuillez vous connecter")
                    $location.path("/Connexion");
                }
            }).catch (function ()  {
                toastr.warning("Veuillez vous connecter")
                    $location.path("/Connexion");
            })

;
        });
    });
    App.config(function ($routeProvider, $locationProvider) {
        $routeProvider
                .when('/Clients', {
                    templateUrl: 'view/Clients.html',
                    controller: 'ClientsCtrl',
                    resolve: function($q, $location,$rootScope) {
                        var deferred = $q.defer(); 
                        deferred.resolve();
                        if (!$rootScope.authenticated) {
                           $location.path('/login');
                        }

                        return deferred.promise;
                      }
      
                })
                .when('/Client/:CustomerId', {
                    templateUrl: 'view/showClient.html',
                    controller: 'showClientCtrl',
                    resolve: function($q, $location, $rootScope) {
                        var deferred = $q.defer(); 
                        deferred.resolve();
                        if (!$rootScope.authenticated) {
                           $location.path('/login');
                        }

                        return deferred.promise;
                      }

                })
                .when('/Connexion', {
                    templateUrl: 'view/login.html',
                    controller: 'LoginCtrl'

                })
                

        $locationProvider.hashPrefix('');

    });


}());