(function () {

    "use strict";

    var App = angular.module("App.controllers", []);

    App.controller("mainCtrl", ["$scope", "ClientsService", "$location","$rootScope","Account","toastr", function ($scope, ClientsService, $location,$rootScope,Account,toastr) {
        $scope.isAuth = function () {
            return $rootScope.authenticated;
        }
        
        $scope.logout = function () {
             Account.logout()
                    .then(function () {
                        toastr.info('Vous avez été déconnecté');
                        $rootScope.authenticated = false;
                        $rootScope.user_id = "";
                        $rootScope.username = "";
                        $location.path('/Connexion');
            });
        }
    }]);

    App.controller("ClientsCtrl", ["$scope", "ClientsService", "$location","$rootScope", function ($scope, ClientsService, $location,$rootScope) {
            $scope.isAuth = function () {
                return $rootScope.authenticated;
            }
            refreshData();
            function refreshData() {
                ClientsService.get().then(function (data) {
                    $scope.listeClients = data.data;
                });

                $scope.showClient = function (CustomerId) {
                    $location.path("/Client/" + CustomerId)
                }
            }

            $scope.addClient = function () {
                ClientsService.add($scope.client).then(function (data) {
                    $scope.client = {};
                    refreshData();
                });
            }
        }]);

    App.controller("showClientCtrl", ["$scope", "$routeParams", "ClientAssurancesService", "ClientsService","$rootScope", 
                   function ($scope, $routeParams, ClientAssurancesService, ClientsService, $rootScope) {
            $scope.isAuth = function () {
                return $rootScope.authenticated;
            }
            $scope.client = {};
            $scope.showEdit = false;

            refreshData();

            function refreshData() {

                $scope.client = {};

                ClientsService.getClient($routeParams.CustomerId).then(function (data) {
                    $scope.client = data.data[0];
                });

                ClientAssurancesService.get($routeParams.CustomerId).then(function (data) {
                    $scope.client.listAssurances = data.data;

                    $scope.client.assuranceVie = false;
                    $scope.client.assuranceDeces = false;

                    for (var i = 0; i < data.data.length; i++) {
                        var assurance = data.data[i];
                        if (assurance.Nom == 'Vie') {
                            $scope.client.assuranceVie = true;
                        } else if (assurance.Nom == 'Décès') {
                            $scope.client.assuranceDeces = true;
                        }
                    }
                });

                ClientAssurancesService.getCars($routeParams.CustomerId).then(function (data) {
                    $scope.client.listCars = data.data;
                });
            }

            $scope.editClient = function () {
                ClientsService.update($scope.client).then(function (data) {
                    $scope.showEdit = false;
                    refreshData();
                });
            }

            $scope.toggleAssu = function (prmTypeAssu) {
                var state = false;
                if ((prmTypeAssu == 'Vie') && ($scope.client.assuranceVie == true)) {
                    state = true;
                } else if ((prmTypeAssu == 'Décès') && ($scope.client.assuranceDeces == true)) {
                    state = true;
                }
                ClientAssurancesService.toggle($routeParams.CustomerId, state, prmTypeAssu).then(function () {
                    refreshData();
                });
            }

            $scope.deleteCar = function (prmIdCar) {
                ClientAssurancesService.carDelete(prmIdCar).then(function () {
                    refreshData();
                });
            }

            $scope.addCar = function (prmIdCar) {
                ClientAssurancesService.carAdd($scope.client).then(function () {
                    $scope.client = {};
                    refreshData();
                });
            }
        }]);

        
    App.controller("LoginCtrl", ["$scope", "toastr" ,"Account","$location", "$rootScope", function ($scope, toastr, Account,$location,$rootScope) {
            $scope.isAuth = function () {
                return $rootScope.authenticated;
            }
            $scope.isAuth()
            $scope.login = function () {
                Account.auth($scope.user).then(function (data){
                    toastr.success('Vous êtes connecté!');
                    $location.path('/Clients');    
                 
                    Account.setuser(data.data)
                    $scope.user = Account.getUser()
                }).catch(function (error) {
                    toastr.error(error.data.msg, error.status);
               });
            };
        }]);


   
}());