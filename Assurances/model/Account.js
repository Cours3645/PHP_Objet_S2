(function() {
    'use strict';
    
    var App = angular.module("App.AccountService", []).factory('Account', Account);


    Account.$inject = ['$log', 'toastr','$http'];

    /* @ngInject */
    function Account($log, toastr,$http) {
        var user = "";
        return {
			setuser: function(data) {
                            user = data;
			},
			getUser: function(profileData) {
                            return user;
			},
                        auth: function (data) {
                            return $http({
                                method: 'POST',
                                url: 'WS/WS_nbp_tarifbas.php?rquest=auth',
                                data: $.param({
                                    data
                                }),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })         
                        },
                        getSession : function () {
                            return $http.get('WS/WS_nbp_tarifbas.php?rquest=getSession').then(function (results) {
                                return results.data;
                            });
                        },
                        logout: function(profileData) {
                            return $http.get('WS/WS_nbp_tarifbas.php?rquest=logout')
			},
		};
    }
}());
