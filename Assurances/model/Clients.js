(function () {
    'use strict';

    var App = angular.module("App.ClientsService", []).factory('ClientsService', clients);

    clients.$inject = ['$log', '$http'];

    function clients($log, $http) {
        var items = [];
        return {
            /*getSessions: function() {
             return $http.post('/services', { 
             type : 'getSource',
             ID    : 'TP001'
             });
             },*/
            get: function () {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=getClients')
            },
            getClient: function (idClient) {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=getClient&idClient=' + idClient)
            },
            update: function (data) {
                return $http({
                    method: 'POST',
                    url: 'WS/WS_nbp_tarifbas.php?rquest=updateCustomer',
                    data: $.param({
                        data
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
            },
            add: function (data) {
                return $http({
                    method: 'POST',
                    url: 'WS/WS_nbp_tarifbas.php?rquest=addCustomer',
                    data: $.param({
                        data
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
            }
        }
    }

}());