(function () {
    'use strict';

    var App = angular.module("App.ClientAssurancesService", []).factory('ClientAssurancesService', clientAssurances);

    clientAssurances.$inject = ['$log', '$http'];

    function clientAssurances($log, $http) {
        var items = [];
        return {
            /*getSessions: function() {
             return $http.post('/services', { 
             type : 'getSource',
             ID    : 'TP001'
             });
             },*/
            get: function (idClient) {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=getCustomerAssurances&idClient=' + idClient)
            },
            getCars: function (idClient) {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=getCustomerCars&idClient=' + idClient)
            },
            toggle: function (idClient, assuranceState, typeAssu) {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=toggleAssu&idClient=' + idClient + '&assuState=' + assuranceState + '&typeAssu=' + typeAssu);
            },
            carDelete: function (idCar) {
                return $http.get('WS/WS_nbp_tarifbas.php?rquest=deleteCar&idCar=' + idCar);
            },
            carAdd: function (data) {
                return $http({
                    method: 'POST',
                    url: 'WS/WS_nbp_tarifbas.php?rquest=addCar',
                    data: $.param({
                        data
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
            }
        }
    }

}());