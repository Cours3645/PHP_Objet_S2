<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personnage
 *
 * @author Corentin
 */
class Personnage {
    private $force;
    private $xp;
    private $nom;
    private $victoire = 0;
    
    public function Personnage($prmForce, $prmXp, $prmNom){
        $this->force = $prmForce;
        $this->xp = $prmXp;
        $this->nom = $prmNom;
        $this->victoire = -1;
    }
    
    public function frapper(){
        return $this->force;
    }
    
    public function frappé($prmCoups){
        $this->xp = $this->xp - $prmCoups;
    }
    
    public function gagner(){
        $this->victoire = 1;
    }
    
    public function mort(){
        $this->xp = 0;
    }
    
    public function getXp(){
        return $this->xp;
    }
    
    public function getVictoire(){
        return $this->victoire;
    }
    
    public function getNom(){
        return $this->nom;
    }
    
}
