<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            include 'classes/Personnage.php';
            
            $objPerso1 = new Personnage(50, 300, "Player 1");
            $objPerso2 = new Personnage(50, 150, "Player 2");
            
            while(($objPerso1->getXp() > 0) && ($objPerso2->getXp() > 0)){
                $objPerso2->frappé($objPerso1->frapper());
                echo 'Il reste ' . $objPerso2->getXp() . 'xp à ' . $objPerso2->getNom() . '<br />';
                if($objPerso2->getXp() <= 0){
                    echo '<br />';
                    break;
                }
                $objPerso1->frappé($objPerso2->frapper());
                echo 'Il reste ' . $objPerso1->getXp() . 'xp à ' . $objPerso1->getNom() . '<br /><br />';
            }
            
            if(($objPerso1->getXp() > 0) && ($objPerso2->getXp() <= 0)){
                $objPerso2->mort();
                echo $objPerso2->getNom() . ' est mort' . '<br />';
                $objPerso1->gagner();
            }else if(($objPerso2->getXp() > 0) && ($objPerso1->getXp() <= 0)){
                $objPerso1->mort();
                echo $objPerso1->getNom() . ' est mort' . '<br />';
                $objPerso2->gagner();
            }
            
            if($objPerso1->getVictoire() == 1){
                echo '<br />Le vainqueur est : ' . $objPerso1->getNom() . '<br />';
            }else if($objPerso2->getVictoire() == 1){
                echo '<br />Le vainqueur est : ' . $objPerso2->getNom() . '<br />';
            }
            
        ?>
    </body>
</html>
