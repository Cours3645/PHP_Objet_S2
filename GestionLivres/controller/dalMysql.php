<?php

/**
 * Description of dalMysql
 *
 * @author Corentin
 */

include 'model/livres.php';

class dalMysql {

    private $DB_SERVER = "localhost";
    private $DB_USER = "CDI";
    private $DB_PASSWORD = "1234";
    private $DB_NAME = "bibliotheque";

    private $objPDO = NULL;


    public function __construct(){
        $this->dbConnect();                    // Initiate Database connection
    }

    /*
    *  Database connection
    */
    private function dbConnect(){
        try {
            $this->objPDO = new PDO ( 'mysql:host=' . $this->DB_SERVER . ';dbname=' . $this->DB_NAME, $this->DB_USER, $this->DB_PASSWORD , array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

            $stmt = $this->objPDO->prepare ( "SET CHARACTER SET utf8" );
            $stmt->execute ();
        } catch ( Exception $e ) {
            echo 'Echec de la connexion à la base de données' . $e;
            exit ();
        }
    }
    
    public function getLivres(){
        
        $result = array();
        
        $SQL = "SELECT * FROM livres";
        
        $stmt = $this->objPDO->prepare ($SQL);
        $stmt->execute ();

        if($stmt->rowCount() > 0){
            
            while($rlt = $stmt->fetch ()){
                $objLivres = new livres($rlt['idLivre'], $rlt['nom']);      
                array_push($result, $objLivres);
            }
            return $result;
        }else{
            return null;
        }
    }
    
    public function addLivre($prmNom){
        
        $SQL = "INSERT INTO `livres` (`idLivre`, `nom`) VALUES (NULL, '" . $prmNom . "')";
        
        $stmt = $this->objPDO->prepare ($SQL);
        $stmt->execute ();
    }
    
    public function majLivre($prmId, $prmNom){
        
        $SQL = "UPDATE `livres` SET `nom` = '" . $prmNom . "' WHERE `livres`.`idLivre` = '" . $prmId . "'";
        
        $stmt = $this->objPDO->prepare ($SQL);
        $stmt->execute ();
    }
    
    public function deleteLivre($prmId){
        
        $SQL = "DELETE FROM livres WHERE idLivre = '" . $prmId . "'";
        
        $stmt = $this->objPDO->prepare ($SQL);
        $stmt->execute ();
    }
}

$db = new dalMysql();

$action = 'read';
if(isset($_GET['action']) && in_array($_GET['action'], array('create', 'read', 'update', 'delete')))
{
  $action = $_GET['action'];
}

switch($action)
{
  case 'read':
        $livres = $db->getLivres();
        break;
  case 'create':
        $db->addLivre($_POST['nom']);
        header('Location: ../index.php');
        break;
  case 'update':
        $db->majLivre($_POST['idLivre'] ,$_POST['nom']);
        header('Location: ../index.php');
        break;
  case 'delete':
        $db->deleteLivre($_GET['idLivre']);
        header('Location: ../index.php');
        break;
}
