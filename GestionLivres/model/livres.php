<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of livres
 *
 * @author Corentin
 */
class livres {
    private $idLivre;
    private $nom;
    
    public function livres($prmId, $prmNom){
        $this->idLivre = $prmId;
        $this->nom = $prmNom;
    }
    
    public function getIdLivre(){
        return $this->idLivre;
    }
    
    public function getNom(){
        return $this->nom;
    }
}
