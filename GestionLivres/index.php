<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title></title>
    </head>
    <body>
        <table class='table table-striped table-bordered'>
            <thead>
                <tr>
                    <th>Id du livre</th>
                    <th>Nom du livre</th>
                    <th></th>
                </tr>
            </thead>
            <?php
                include 'controller/dalMysql.php';

                for($i = 0; $i < count($livres); $i++){
                    $livre = $livres[$i];
                    
                    $newLigne = "<tr>";
                    $newLigne .= "<td>";
                    $newLigne .= $livre->getIdLivre();
                    $newLigne .= "</td>";
                    $newLigne .= "<td>";
                    $newLigne .= $livre->getNom();
                    $newLigne .= "</td>";
                    $newLigne .= "<td>";
                    $newLigne .= "<a href='majLivre.php?action=update&idLivre=" . $livre->getIdLivre() . "&nom=" . $livre->getNom() . "' class='glyphicon glyphicon-pencil'></a>";
                    $newLigne .= "  ";
                    $newLigne .= "<a href='controller/dalMysql.php?action=delete&idLivre=" . $livre->getIdLivre() . "' class='glyphicon glyphicon-trash'></a>";
                    $newLigne .= "</td>";
                    $newLigne .= "</tr>";
                    
                    echo $newLigne;
                }
            ?>
        </table>
        
        <form action='controller/dalMysql.php?action=create' method='post'>
            <label class="control-label" for="nom">Nom :</label>
            <input type='text' name='nom' placeholder='Nom du livre' />
            <button type="submit" class="btn btn-default">Enregistrer</button>
        </form>
    </body>
</html>
